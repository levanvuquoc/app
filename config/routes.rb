Rails.application.routes.draw do
  resources :categories
  resources :products

  root :to => "products#index"

  get     "login"    => "session#new"
  post    "login"    => "session#create"
  delete  "logout"   => "session#destroy"

  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
