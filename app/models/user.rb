class User < ApplicationRecord

  def admin?
    role == "admin"
  end

  def authenticate(pass)
    password == pass
  end
end
