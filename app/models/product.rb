class Product < ApplicationRecord
  has_many :enrollments
  has_many :category, through: :enrollments
  validates :name, length: { minimum: 3 }
end
