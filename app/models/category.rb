class Category < ApplicationRecord
  has_many :enrollments
  has_many :product, through: :enrollments
  validates :name, length: { minimum: 3 }
end
