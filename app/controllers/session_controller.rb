class SessionController < ApplicationController
  def new
  end

  def create
    user = User.find_by name: params[:session][:name].downcase
    if user && user.authenticate(params[:session][:password]) 
      flash[:success] = "Login success"
      log_in user
      if user.admin?
        redirect_to users_path
      else
        redirect_to user
      end
    else
    flash[:danger] = "Invalid email/password combination"
    render :new
    end
  end

  def destroy
    log_out
    flash[:success] = "You are logged out"
    redirect_to login_path
  end
end
